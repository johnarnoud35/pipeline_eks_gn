import axios from "axios";

const baseURL = "backend.service:8000"; // url à changer lors du deploiement

const axiosInstance = axios.create({
  baseURL,
});

axiosInstance.interceptors.response.use(
  (response) => response,
  (error) => Promise.reject(error)
);

export default axiosInstance;